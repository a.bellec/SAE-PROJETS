import java.util.ArrayList;
import java.util.Collections;

/**
 * Autors : Alan Bellec, Camille Gouault-- Lamour
 * 
 */
class Grundy2RecGplusGequalsP {

    long cpt; // counter
    Grundy2RecBrute monGrundy2RecBrute = new Grundy2RecBrute();
    ArrayList<ArrayList<Integer>> posPerdantes = new ArrayList<ArrayList<Integer>>(); //collection for winning positions
    ArrayList<ArrayList<Integer>> posGagnantes = new ArrayList<ArrayList<Integer>>(); //collection for loosing positions

    void principal(){
        //testchangementJoueur();
        //testTourJoueur();
        //testTourIA();
        //partie();
        testEstGagnanteEfficacite();
        //testEstConnuePerdante();
        //testEstConnueGagnante();
        //testNormalise();
        //testSontIdentiques();
        //testSimplification();
        //testSimplifie();
    }

    /**
     * Method that removes losing piles and simplifies winning pairs
     * @param jeu collection of items
     * @return 0 if the game is losing, 1 if the game is winning and -1 if the game is over 50
     */
    int simplifie(ArrayList<Integer>jeu ){
        //System.out.println("jeu dans simplifie : " + jeu.toString());
        int perdant = 0;
        boolean sup50 = false;
        int [] type = {0,0,0,1,0,2,1,0,2,1,0,2,1,3,2,1,3,2,4,3,0,4,3,0,4,3,0,4,1,2,3,1,2,4,1,2,4,1,2,4,1,5,4,1,5,4,1,5,4,1,0};
        //System.out.println("Dans simplifie()");
        int tmpi,tmpj;
        tmpi = 0;
        tmpj = 0;
        
        ArrayList<Integer> jeuSimplifie = new ArrayList<Integer>();
        jeuSimplifie = jeu;

        if (jeuSimplifie.size() == 1) {
            tmpi = jeuSimplifie.get(0);
            if ( tmpi <=50){
                for (int m = 0; m < type.length; m++) {
                    if (type[tmpi] > 0) {
                        perdant = 1;
                    }
                }
            } else{
                perdant = -1;
                sup50 = true;
            }
        } else {
            for(int i = 0; i < jeu.size() && !sup50;i++){
                tmpi = jeu.get(i);
                if(tmpi <= 50){
                    for(int j = i+1; j < jeu.size(); j++){
                            
                        tmpj = jeu.get(j);
                        if (tmpj <= 50){  
                            if(type[tmpi] != 0 && type[tmpi] == type[tmpj]){
                                //perdant
                                    
                                jeuSimplifie.remove(j);
                                j--;
                            } else if ( type[tmpi] == 0 && type[tmpj] == 0){
                                //perdant = true;
                            }
                            else if ( type[tmpi] != 0 && type[tmpj] == 0){
                                perdant++;
                            }
                            else if ( type[tmpi] == 0 && type[tmpj] != 0){
                                perdant++;
                            }
                            else  if ((type[tmpi] != 0 && type[tmpj] != 0) && type[tmpi] != type[tmpj]){
                                perdant++;
                            }
                        }else {
                            perdant = -1;
                            sup50 = true;
                        }
                    }
                }
                else {
                    perdant = -1;
                    sup50 = true;
                }
            }
            if ( perdant > 0){
                perdant = 1;
            }
        }
                
        return perdant;
    }

    /**
     * simplifie's test method
     */
    void testSimplifie(){
        System.out.println("\n ***testGplusGequals()****");
        ArrayList<Integer> jeu = new ArrayList<Integer>();
        ArrayList<Integer> res1 = new ArrayList<Integer>();
        ArrayList<Integer> res2 = new ArrayList<Integer>();
        jeu.add(5);
        jeu.add (8);
        jeu.add(3);
        jeu.add(13);
        jeu.add(9);
        res1.add(5);
        res1.add(3);
        res1.add(13);
        testCasSimplifie(jeu,1);
        res2 = res1;
        res2.add(4);
        res2.add(10);
        jeu.add(4);
        jeu.add(10);
        testCasSimplifie(jeu,1);
        jeu.clear();
        jeu.add(7);
        testCasSimplifie(jeu,0);
        jeu.clear();
        jeu.add(23);
        jeu.add(26);
        testCasSimplifie(jeu,0);
        jeu.clear();
        jeu.add(24);
        testCasSimplifie(jeu,1);
        jeu.clear();
        jeu.add(13);
        testCasSimplifie(jeu, 1);
        
        // CAS LIMITE
        jeu.clear();
        jeu.add(55);
        testCasSimplifie(jeu, -1);
        jeu.clear();
        jeu.add(100);
        jeu.add(100);
        testCasSimplifie(jeu,-1);
        jeu.clear();
        testCasSimplifie(jeu,0);
    }

    /**
     * Method to test a case of testSimplifie's method
     * @param jeu
     * @param result
     */
    void testCasSimplifie(ArrayList<Integer> jeu, int result){
            // Arrange
            System.out.print("Simplifie  (" + jeu.toString() + "), résultat attendu : " + result);

            // Act
            int resExec = simplifie(jeu);
            System.out.println("resExec : " + resExec);
    
            // Assert
            if (resExec == result) {
                System.out.println("OK\n");
            } else {
                System.err.println("ERREUR\n");
            }
    }
    
    /**
	 * Method allowing to delete items of a board
	 * if they are already known from sitPerdantes
	 * @param jeu game board
	 * @return a list in which all losing situations are deleted
	 */
	ArrayList<Integer>simplification(ArrayList<Integer> jeu){ 
        ArrayList<Integer>liste1 = new ArrayList<Integer>();
        ArrayList<Integer> ret = normalise(jeu);
        int i = 0;
        
        while( i < ret.size()){
			liste1.clear();
			liste1.add(ret.get(i));
			if (estConnuePerdante(liste1)){
				ret.remove(i);
			}
			i++;
		}
        return ret;
    }
	
	
	/**
	 * Method allowing to test simplification
	 */
	void testSimplification(){
		// Basic cases
		System.out.println("\n***testSimplification : ");
		System.out.println("Liste n°1 : ");
		ArrayList<Integer>j1 = new ArrayList<Integer>();
		ArrayList<Integer>r1 = new ArrayList<Integer>();
		j1.add(2);
		j1.add(10);
		ArrayList<Integer>p1 = new ArrayList<Integer>();
		ArrayList<Integer>p6 = new ArrayList<Integer>();
		p1.add(2);
		p6.add(10);
		posPerdantes.add(p1);
		posGagnantes.add(p6);
		r1.add(10);
		testCasSimplification(j1,r1);
		posGagnantes.clear();
		posPerdantes.clear();
		
		System.out.println("Liste n°2 : ");
		ArrayList<Integer>j2 = new ArrayList<Integer>();
		ArrayList<Integer>r2 = new ArrayList<Integer>();
		j2.add(78);
		j2.add(60);
		ArrayList<Integer>p4 = new ArrayList<Integer>();
		p4.add(90);
		p4.add(61);
		r2.add(60);
		r2.add(78);
		posPerdantes.add(p4);
		testCasSimplification(j2,r2);
		posPerdantes.clear();
	}
	
    
    /**
     * Allows to test several cases of simplification
     * @param liste collection of items
     * @param ret integer expected after method
     */
    void testCasSimplification(ArrayList<Integer> liste,ArrayList<Integer>ret){
        // Arrange
        System.out.println ("Liste au départ : " + liste.toString() +" Liste attendu : " + ret.toString());
		// Act
        ArrayList<Integer>resExec = new ArrayList<Integer>();
        resExec = simplification(liste);
        // Assert			 
        if ( resExec.equals(ret)){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
            System.out.println(resExec);
        }
    }
          
    
    

     /**
     * Play the winning move if it exists
     * 
     * @param jeu board
     * @return true if there is a winning move, false otherwise
     */
    boolean jouerGagnant(ArrayList<Integer> jeu) {
        boolean gagnant = false;
        if (jeu == null) {
            System.err.println("suivant(): le paramètre jeu est null");
        } else {
            ArrayList<Integer> essai = new ArrayList<Integer>();
            int ligne = premier(jeu, essai);
			// implementation of rule number 2
			// A situation (or position) is said to be winning for the machine (or the player, whatever), if there is AT LEAST ONE 
			// decomposition (i.e. ONE action which consists in breaking a pile into 2 unequal piles) which is losing for the opponent.
            while (ligne != -1 && !gagnant) {
                if (estPerdante(essai)) {
                    jeu.clear();
                    gagnant = true;
                    for (int i = 0; i < essai.size(); i++) {
                        jeu.add(essai.get(i));
                    }
                } else {
                    ligne = suivant(jeu, essai, ligne);
                }

            }
        }
		
        return gagnant;
    }
	
	/**
     * RECURSIVE method which indicates if the configuration (of the current jeu or test jeu) is losing
     * 
     * @param jeu current jeu board (the state of the jeu at a certain time during the jeu)
     * @return true if the (jeu) configuration is losing, false otherwise
     */
    boolean estPerdante(ArrayList<Integer> jeu) {
        //System.out.println("dans estPerdante jeu : " + jeu.toString());
        boolean ret = true; // by default the configuration is losing
        if (jeu == null) {
            System.err.println("estPerdante(): le paramètre jeu est null");
        }
        else {
            jeu = simplification(jeu);
            //System.out.println("dans else");
            // if there are only piles of 1 or 2 matches left on the board
            // then the situation is necessarily losing (ret=true) = END of recursion
            if ( !estPossible(jeu) ) {
                ret = true;
            }
            // if the game board (passed as a parameter) is KNOWN to be LOSING
            // then we immediately return "true" and we stop the recursion (so we save time!)
            else if (simplifie (jeu) == 1) {
                //System.out.println("simplifie(jeu) : "+simplifie(jeu));
                ret = false;
            }
            else if (simplifie (jeu) == 0) {
                //System.out.println("simplifie(jeu) : "+simplifie(jeu));
                ret = true;
                //System.out.println("ret = "+ret);
            }
            else if ( estConnuePerdante ( jeu ) ) {
                ret = true;
            } 
            else if ( estConnueGagnante(jeu)) {
                ret = false;
            }
            
            // otherwise you have to continue decomposing the heaps by creating the following test set
            else {
                //System.out.println("Est perdante : dans else");
                // creation of a test set that will examine all possible decompositions
                // possible decompositions from the game
                ArrayList<Integer> essai = new ArrayList<Integer>(); // size = 0
 
                // first decomposition: remove 1 match from the first pile that has
                // at least 3 matches, line = -1 means that there are no more piles with at least 3 matches
                int ligne = premier(jeu, essai);
 
                while ( (ligne != -1) && ret) {
                    //System.out.println("dans while");
                    // implementation of rule number 1
                    // A situation (or position) is said to be losing for the machine (or the player, whatever) if and only if ALL
                    // its possible decompositions (i.e. ALL the actions which consist in decomposing a pile into 2 unequal piles) are
                    // ALL win for the opponent.
                    // If ONLY ONE decomposition (from the game) is losing (for the opponent) then the configuration is NOT losing.
                    // Here the call to "isLosing" is RECURRENT.
                    if (estPerdante(essai) == true) {
                        ret = false;
                    } else {
                        // generates the following test configuration (i.e. ONE possible decomposition)
                        // from the set, if line = -1 there is no more possible decomposition
                        ligne = suivant(jeu, essai, ligne);
                    }
                }
                // if the "essai" configuration is losing (e.g. [4] or [3, 3] or [3, 3, 4]) then it is
                // added to the table of losing positions
                if (ret){
                    //System.out.println("AVANT posPerdantes dans else { if ret : " + posPerdantes.toString());
                    //System.out.println("else { if ret");
                    essai = normalise(jeu);
                    posPerdantes.add ( essai );
                    //System.out.println("APRES posPerdantes dans else { if ret : " + posPerdantes.toString());
                }
                if(!ret) {
                    //System.out.println("else { if !ret");
                    essai = normalise(jeu);
                    posGagnantes.add(essai);
                }
            }
        }
        cpt++;
		//System.out.println("posGagnantes : " + posGagnantes.toString());
        //System.out.println("posPerdantes : " + posPerdantes.toString());
        //System.out.println("Avant return : ret = "+ret);
        return ret;
    }

    
	
	/**
     * Indicates whether the configuration is winning.
	 * Method that simply calls "isLosing".
     * 
     * @param jeu board
     * @return true if the configuration is winning, false otherwise
     */
    boolean estGagnante(ArrayList<Integer> jeu) {
        boolean ret = false;
        if (jeu == null) {
            System.err.println("estGagnante(): le paramètre jeu est null");
        } else {
            ret = !estPerdante(jeu);
            //System.out.println("dans estGagnante() ret :" + ret);
        }
        return ret;
    }

    /**
     * Indicates the efficiency of estGagnante
     */
    void testEstGagnanteEfficacite() {
        
        System.out.println("\n");
        System.out.println("*** Test de l'efficacité de estGagnante() ***");
        // local variables
        int n; 
        boolean indice;
        long t1, t2, diffT;
        ArrayList <Integer> eff = new ArrayList<Integer>();

        // initialisation
        n = 3;
        // 50 experiences
        for (int i = 1; i <= 98; i++){
            eff.add(n);
            //System.out.println("\nNombre d'allumettes n = " + n);

            cpt = 0;
            t1 = System.nanoTime();
            indice = estGagnante(eff);
            System.out.println("Indice pour "+n+ "= " + indice);
            //System.out.println("posGagnantes : " + posGagnantes.toString());
            //System.out.println("posPerdantes : " + posPerdantes.toString());
            t2 = System.nanoTime();
            diffT = (t2 - t1); //en nanoseconds

            //System.out.println("Tps = " + diffT + " ns");
            //System.out.println("Après appel de la méthode, compteur cpt = " + cpt);
            System.out.println(n + " " + diffT + " " + cpt);

            n++;
            
            posPerdantes.clear();
            posGagnantes.clear();
            eff.clear();
        }
    }

    /**
     * Brief tests of the playerWinner() method
     */
    void testJouerGagnant() {
        System.out.println();
        System.out.println("*** testJouerGagnant() ***");

        System.out.println("Test des cas normaux");
        ArrayList<Integer> jeu1 = new ArrayList<Integer>();
        jeu1.add(6);
        ArrayList<Integer> resJeu1 = new ArrayList<Integer>();
        resJeu1.add(4);
        resJeu1.add(2);
		
        testCasJouerGagnant(jeu1, resJeu1, true);
        
    }

    /**
     * Testing a case of the playWinner() method
	 *
	 * @param jeu the jeu board
	 * @param resJeu the jeu board after playing win
	 * @param res the result expected by playWinner
     */
    void testCasJouerGagnant(ArrayList<Integer> jeu, ArrayList<Integer> resJeu, boolean res) {
        // Arrange
        System.out.print("jouerGagnant (" + jeu.toString() + ") : ");

        // Act
        boolean resExec = jouerGagnant(jeu);

        // Assert
        System.out.print(jeu.toString() + " " + resExec + " : ");
        if (jeu.equals(resJeu) && res == resExec) {
            System.out.println("OK\n");
        } else {
            System.err.println("ERREUR\n");
        }
    }	

    /**
     * Divide the matches in a line into two piles (1 line = 1 pile)
     * 
     * @param jeu array of matches per line
     * @param ligne line (heap) on which the matches must be separated
     * @param nb number of matches REMOVED from the line after separation
     */
    void enlever ( ArrayList<Integer> jeu, int ligne, int nb ) {
		// traitement des erreurs
        if (jeu == null) {
            System.err.println("enlever() : le paramètre jeu est null");
        } else if (ligne >= jeu.size()) {
            System.err.println("enlever() : le numéro de ligne est trop grand");
        } else if (nb >= jeu.get(ligne)) {
            System.err.println("enlever() : le nb d'allumettes à retirer est trop grand");
        } else if (nb <= 0) {
            System.err.println("enlever() : le nb d'allumettes à retirer est trop petit");
        } else if (2 * nb == jeu.get(ligne)) {
            System.err.println("enlever() : le nb d'allumettes à retirer est la moitié");
        } else {
			// new heap (box) added to the jeu (necessarily at the end of the table)
			// this new pile contains the number of matches removed (nb) from the pile to be separated			
            jeu.add(nb);
			// the remaining pile with "nb" matches less
            jeu.set(ligne, jeu.get(ligne) - nb);
        }
    }

    /**
     * Test if it is possible to separate one of the piles
     * 
     * @param jeu board
     * @return true if there is at least one pile of 3 or more matches, false otherwise
     */
    boolean estPossible(ArrayList<Integer> jeu) {
        boolean ret = false;
        if (jeu == null) {
            System.err.println("estPossible(): le paramètre jeu est null");
        } else {
            int i = 0;
            while (i < jeu.size() && !ret) {
                if (jeu.get(i) > 2) {
                    ret = true;
                }
                i = i + 1;
            }
        }
        return ret;
    }

    /**
     * Create a very first test configuration from the jeu
     * 
     * @param jeu board
     * @param jeuEssai new jeu configuration
     * @return the number of the pile divided in two or (-1) if there is no pile of at least 3 matches
     */
    int premier(ArrayList<Integer> jeu, ArrayList<Integer> jeuEssai) {
	
        int numTas = -1; // no heap to separate by default
		int i;
		
        if (jeu == null) {
            System.err.println("premier(): le paramètre jeu est null");
        } else if (!estPossible((jeu)) ){
            System.err.println("premier(): aucun tas n'est divisible");
        } else if (jeuEssai == null) {
            System.err.println("estPossible(): le paramètre jeuEssai est null");
        } else {
            // before copying the jeu to Testjeu there is a Testjeu reset 
            jeuEssai.clear();
            i = 0;
			
			// copy square by square
			// testjeu is the same as the original jeu
            while (i < jeu.size()) {
                jeuEssai.add(jeu.get(i));
                i = i + 1;
            }
			
            i = 0;
			// search for a pile of matches of at least 3 matches in the jeu
			// otherwise numTas = -1
			boolean trouve = false;
            while ( (i < jeu.size()) && !trouve) {
				
				// si on trouve un tas d'au moins 3 allumettes
				if ( jeuEssai.get(i) >= 3 ) {
					trouve = true;
					numTas = i;
				}
				
				i = i + 1;
            }
			
			// separates the pile (numTas box) into a pile of ONE match at the end of the table 
			// the pile in the numTas box has decreased by one match (removal of one match)
			// testjeu is the jeu board that shows this separation
            if ( numTas != -1 ) enlever ( jeuEssai, numTas, 1 );
        }
		
        return numTas;
    }

    /**
     * Brief tests of the premier() method
     */
    void testPremier() {
        System.out.println();
        System.out.println("*** testPremier()");

        ArrayList<Integer> jeu1 = new ArrayList<Integer>();
        jeu1.add(10);
        jeu1.add(11);
        int ligne1 = 0;
        ArrayList<Integer> res1 = new ArrayList<Integer>();
        res1.add(9);
        res1.add(11);
        res1.add(1);
        testCasPremier(jeu1, ligne1, res1);
    }


    /**
     * Test a case of the testFirst method
	 * @param jeu the jeu board
	 * @param ligne the number of the heap separated first
	 * @param res the jeu board after a first split
     */
    void testCasPremier(ArrayList<Integer> jeu, int ligne, ArrayList<Integer> res) {
        // Arrange
        System.out.print("premier (" + jeu.toString() + ") : ");
        ArrayList<Integer> jeuEssai = new ArrayList<Integer>();
        // Act
        int noLigne = premier(jeu, jeuEssai);
        // Assert
        System.out.println("\nnoLigne = " + noLigne + " jeuEssai = " + jeuEssai.toString());
        if (jeuEssai.equals(res) && noLigne == ligne) {
            System.out.println("OK\n");
        } else {
            System.err.println("ERREUR\n");
        }
    }

    /**
     * Test a case of the testFirst method
        copy.clear();;
        testCasualty(copy,copy,true);

     * @param jeu jeu board
     * @param jeuEssai configuration of the jeu after separation
     * @param ligne the number of the heap that was last split
     * @return the number of the heap divided in two for the new configuration, -1 if no more decomposition is possible
     */
    int suivant(ArrayList<Integer> jeu, ArrayList<Integer> jeuEssai, int ligne) {
	
        // System.out.println("suivant(" + jeu.toString() + ", " +jeuEssai.toString() +
        // ", " + ligne + ") = ");
		
		int numTas = -1; // by default there is no more decomposition possible
		
        int i = 0;
		// error handling
        if (jeu == null) {
            System.err.println("suivant(): le paramètre jeu est null");
        } else if (jeuEssai == null) {
            System.err.println("suivant() : le paramètre jeuEssai est null");
        } else if (ligne >= jeu.size()) {
            System.err.println("estPossible(): le paramètre ligne est trop grand");
        }
		
		else {
		
			int nbAllumEnLigne = jeuEssai.get(ligne);
			int nbAllDernCase = jeuEssai.get(jeuEssai.size() - 1);
			
			// if on the same line (passed in parameter) we can still remove matches,
			// i.e. if the difference between the number of matches on this line and
			// the number of matches at the end of the array is > 2, then we remove another
			// 1 match on this line and add 1 match at the last cell		
            if ( (nbAllumEnLigne - nbAllDernCase) > 2 ) {
                jeuEssai.set ( ligne, (nbAllumEnLigne - 1) );
                jeuEssai.set ( jeuEssai.size() - 1, (nbAllDernCase + 1) );
                numTas = ligne;
            } 
			
			// if not, the next heap (line) in the jeu must be examined for possible breakdown
			// a new test configuration is recreated, identical to the jeu board
			else {
                // copy of the jeu in JeuEssai
                jeuEssai.clear();
                for (i = 0; i < jeu.size(); i++) {
                    jeuEssai.add(jeu.get(i));
                }
				
                boolean separation = false;
                i = ligne + 1; // tas suivant
				// if there is still a pile and it contains at least 3 matches
				// then a first separation is made by removing 1 match
                while ( i < jeuEssai.size() && !separation ) {
					// the pile must be at least 3 matches long
                    if ( jeu.get(i) > 2 ) {
                        separation = true;
						// we start by removing 1 match from this pile
                        enlever(jeuEssai, i, 1);
						numTas = i;
                    } else {
                        i = i + 1;
                    }
                }				
            }
        }
		
        return numTas;
    }

    /**
     * Brief tests of the following() method
     */
    void testSuivant() {
        System.out.println();
        System.out.println("*** testSuivant() ****");

        int ligne1 = 0;
        int resLigne1 = 0;
        ArrayList<Integer> jeu1 = new ArrayList<Integer>();
        jeu1.add(10);
        ArrayList<Integer> jeuEssai1 = new ArrayList<Integer>();
        jeuEssai1.add(9);
        jeuEssai1.add(1);
        ArrayList<Integer> res1 = new ArrayList<Integer>();
        res1.add(8);
        res1.add(2);
        testCasSuivant(jeu1, jeuEssai1, ligne1, res1, resLigne1);

        int ligne2 = 0;
        int resLigne2 = -1;
        ArrayList<Integer> jeu2 = new ArrayList<Integer>();
        jeu2.add(10);
        ArrayList<Integer> jeuEssai2 = new ArrayList<Integer>();
        jeuEssai2.add(6);
        jeuEssai2.add(4);
        ArrayList<Integer> res2 = new ArrayList<Integer>();
        res2.add(10);
        testCasSuivant(jeu2, jeuEssai2, ligne2, res2, resLigne2);

        int ligne3 = 1;
        int resLigne3 = 1;
        ArrayList<Integer> jeu3 = new ArrayList<Integer>();
        jeu3.add(4);
        jeu3.add(6);
        jeu3.add(3);
        ArrayList<Integer> jeuEssai3 = new ArrayList<Integer>();
        jeuEssai3.add(4);
        jeuEssai3.add(5);
        jeuEssai3.add(3);
        jeuEssai3.add(1);
        ArrayList<Integer> res3 = new ArrayList<Integer>();
        res3.add(4);
        res3.add(4);
        res3.add(3);
        res3.add(2);
        testCasSuivant(jeu3, jeuEssai3, ligne3, res3, resLigne3);

    }

    /**
     * Test a case of the following method
	 * 
	 * @param jeu the jeu board
	 * @param jeuEssai the jeu board obtained after splitting a heap
	 * @param ligne is the number of the heap that was last split
	 * @param resJeu is the expected test jeu after splitting
	 * @param resLigne is the expected number of the heap that is split
     */
    void testCasSuivant(ArrayList<Integer> jeu, ArrayList<Integer> jeuEssai, int ligne, ArrayList<Integer> resJeu, int resLigne) {
        // Arrange
        System.out.print("suivant (" + jeu.toString() + ", " + jeuEssai.toString() + ", " + ligne + ") : ");
        // Act
        int noLigne = suivant(jeu, jeuEssai, ligne);
        // Assert
        System.out.println("\nnoLigne = " + noLigne + " jeuEssai = " + jeuEssai.toString());
        if (jeuEssai.equals(resJeu) && noLigne == resLigne) {
            System.out.println("OK\n");
        } else {
            System.err.println("ERREUR\n");
        }
    }
	
    /**
     * Determines if the configuration is known to be a loser in posPerdantes
     * 
     * @param jeu the jeu board
     * @return true if the jeu is in posPerdantes
     */
    boolean estConnuePerdante ( ArrayList<Integer> jeu ) {
        // création d'une copie de jeu triée sans 1, ni 2
        //System.out.println("dans estConnuePerdante");
        ArrayList<Integer> copie = normalise(jeu);
        boolean ret = false;
        int i = 0;
        while ( !ret && i < posPerdantes.size() ) {
            ArrayList<Integer> t = normalise(posPerdantes.get(i));
            if (sontIdentiques(copie, posPerdantes.get(i))) {
                ret = true;
            }
            i++;;
        }
        return ret;
    }

    /**
     * Method of testing estConnuePerdante
     */
    void testEstConnuePerdante() {
        System.out.println("\n *** test EstConnuePerdante () ***");
        ArrayList<Integer> copie = new ArrayList<Integer>();
       
        copie.add(7);
        copie.add(10);
        copie.add(4);
        posPerdantes.add(copie);
        
        testCasEstConnuePerdante(copie,true);
    }

    /**
     * Method of testing estConnuePerdante
     * @param copie collection of integers items
     * @param ret result of the execution of copie by estConnuePerdante
     */
    void testCasEstConnuePerdante(ArrayList<Integer> copie,boolean ret ){
            // Arrange
            System.out.print ("ArrayList <Integer> copie : " + copie.toString()+ " Resultat attendu : "+ ret +")" );
            // Act
            boolean resExec;
            resExec = estConnuePerdante(copie);
            System.out.println("posPerdantes : " + posPerdantes.toString());
            // Assert			 
            if ( resExec == ret ){
                System.out.println ("OK");
            } else {
                System.err.println ("ERREUR");
            }
    }

    /**
     * Determines if the configuration is known to be a winner in posWinners
     * 
     * @param jeu the game board
     * @return true if the game is in posWinners
     */
    boolean estConnueGagnante( ArrayList<Integer> jeu ) {
        //System.out.println("dans estConnueGagnante");
        // création d'une copie de jeu triée sans 1, ni 2
        ArrayList<Integer> copie = normalise(jeu);
        boolean ret = false;
        int i = 0;
        while ( !ret && i < posGagnantes.size() ) {
          //Changement Jean 
            ArrayList<Integer> t = normalise(posGagnantes.get(i));
            if (sontIdentiques(copie, t)) {
                ret = true;
            }
            i++;
        }
        return ret;
    }
    /**
     * estConnueGagnante's test method
     */
    void testEstConnueGagnante() {
        System.out.println("\n *** test estConnueGagnante () ***");
        ArrayList<Integer> copie = new ArrayList<Integer>();
        copie.add(1);
        copie.add(1);
        copie.add(1);
        copie.add(5);
        posGagnantes.add(copie);

        
        testCasEstConnueGagnante(copie,true);
    }

    /**
     * Test a case of the estConnueGagnante's test method
     * @param copie collection of items
     * @param ret result of the call of copie by estConnueGagnante
     */
    void testCasEstConnueGagnante(ArrayList<Integer> copie,boolean ret ){
            // Arrange
            System.out.print ("ArrayList <Integer> copie : " + copie.toString()+ " Resultat attendu : "+ ret +")" );
            // Act
            boolean resExec;
            resExec = estConnueGagnante(copie);
            System.out.println("posGagnantes : " + posGagnantes.toString());
            // Assert			 
            if ( resExec == ret ){
                System.out.println ("OK");
            } else {
                System.err.println ("ERREUR");
            }
    }
    

    /**
     * Method to normalise ( sort and delete the 1 and 2 )
     * @param jeu collection of items
     * @return a copy of the jeu without the 1 and 2 an sorted
     */

    ArrayList<Integer> normalise(ArrayList<Integer> jeu){
        //System.out.println("dans normalise : jeu : " + jeu.toString());
        ArrayList<Integer> normal = new ArrayList<Integer>();
        for(int i = 0;i<jeu.size();i++){
            if(jeu.get(i)>2){
                normal.add(jeu.get(i));
            }
        }
        Collections.sort(normal);
        //System.out.println("normal : " + normal.toString());
        return normal;
    }
    /**
     * Normalise's test Method
     * 
	*/
    void testNormalise(){
        System.out.println("\n *** testNormalise() ***");
        ArrayList<Integer> jeu = new ArrayList<Integer>();
        ArrayList<Integer> result = new ArrayList<Integer>();
        jeu.add(9);
        jeu.add(1);
        jeu.add(2);
        jeu.add(3);
        result.add(3);
        result.add(9);
        testCasNormalise(jeu, result);
        jeu.clear();
        result.clear();
        jeu.add(9);
        jeu.add(1);
        jeu.add(8);
        jeu.add(1);
        result.add(8);
        result.add(9);
        testCasNormalise(jeu, result);
    }
    /**
     * 
     * Test a case of the normalise's test method
     * 
     * @param jeu collection of items
     * @param result result of the call of jeu by normalise
     */
    void testCasNormalise(ArrayList<Integer> jeu, ArrayList<Integer> result){
        // Arrange
        System.out.println ("Normalise (" + jeu.toString() +", resultat attendu : "+ result.toString()+")");
		// Act
        ArrayList<Integer> resExec = new ArrayList<Integer>();
        resExec = normalise(jeu);
        System.out.println("Résultat : " + resExec.toString());
        // Assert			 
        if (resExec.equals(result)){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }

    /**
     * Checks if 2 ArrayList<Integer> are identical 2 by 2
     * @param copie 
     * @param info
     * @return true if the 2 ArrayList entered as parameters are identical, false otherwise
     */
    boolean sontIdentiques(ArrayList<Integer> copie, ArrayList<Integer> info){
        boolean ret = false;
        int i = 0;
        int j = 0;
        ArrayList<Integer> test = new ArrayList<>(info);
        if(copie.size() == test.size()){ // 3-3-4 posper 4-3-5 copie
            do{
                ret = false;
                j = 0;
                while(j < test.size() && ret == false){
                    if(copie.get(i) == test.get(j)){
                        ret = true;
                        test.remove(j);
                    } else {
                        j++;
                    }
                }
                i++;
            } while(i < copie.size() && ret);
        }

        return ret;
    }

    /**
     * SontIdentiques's test method
     */
    void testSontIdentiques(){
        System.out.println("\n *** testSontIdentiques() ***");
        ArrayList<Integer> copie = new ArrayList<Integer>();
        ArrayList<Integer> positionsA= new ArrayList<Integer>();
        ArrayList<Integer> positionsB= new ArrayList<Integer>();

        copie.add(9);
        copie.add(3);
        copie.add(5);
        copie.add(4);
        positionsA.add(10);
        positionsA.add(4);
        positionsA.add(2);
        positionsA.add(7);

        positionsB.add(7);
        positionsB.add(10);
        positionsB.add(4);
        positionsB.add(2);

        testCasSontIdentiques(copie,copie,true);
        testCasSontIdentiques(copie,positionsA,false);
        testCasSontIdentiques(copie,positionsB,false);
        testCasSontIdentiques(positionsA,positionsB,true);
        testCasSontIdentiques(positionsB, positionsB, true);

    }

     /**
     * Test a case of the sontIdentiques's test method
     * @param copie collection of items
     * @param positionsPerdantes collection of loosing positions
     * @param ret result of the call of copie and positionsPerdantes by sontIdentiques
     */
    void testCasSontIdentiques(ArrayList<Integer> copie, ArrayList<Integer> positionsPerdantes,boolean ret){
        // Arrange
        System.out.print ("ArrayList <Integer> copie : " + copie.toString() +" ArrayList<Integer> positionsPerdantes : " + positionsPerdantes.toString());
		// Act
        boolean resExec;
        resExec = sontIdentiques(copie,positionsPerdantes);
        // Assert			 
        if ( resExec == ret ){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }
}

