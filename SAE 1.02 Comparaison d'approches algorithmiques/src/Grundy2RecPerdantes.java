import java.util.ArrayList;
import java.util.Collections;
/**
 * Autors : Alan Bellec, Camille Gouault-- Lamour
 * 
 */
class Grundy2RecPerdantes {

    long cpt; 
    Grundy2RecBrute monGrundy2RecBrute = new Grundy2RecBrute();
    ArrayList<ArrayList<Integer>> posPerdantes = new ArrayList<ArrayList<Integer>>();

    void principal(){
        
        //testAffichageJeuDeDepart();
        //testchangementJoueur();
        //testTourJoueur();
        //testTourIA();
        //partie();
        testEstGagnanteEfficacite();
        //testNormalise();
        //testSontIdentiques();

    }

    /**
     * Displays the total number of sticks
     * @param jeu containing the different sticks in the different lines
     */
    void affichageTas(ArrayList<Integer> jeu){

        for(int i = 0 ; i < jeu.size() ; i++){
            if ( jeu.get(i) != 0 ){
                System.out.print( i + " : ");
                for (int j = 0 ; j < jeu.get(i) ; j++){
                    System.out.print("|" + " ");
                }
                System.out.println();
            }
        }

    }

    /**
     * Method of creating a collection containing the number of sticks in the game
    * @return collection containing the number of sticks
    */
    ArrayList<Integer> jeuDeDepart(){
        
        ArrayList<Integer> jeu = new ArrayList<Integer>();
        int tmp;
        tmp =  SimpleInput.getInt("Entrez le nombre de batons que vous voulez pour la partie (minimum 3) : ");
        while ( tmp < 3 ){
            tmp = SimpleInput.getInt("Entrez le nombre de batons que vous voulez pour la partie (minimum 3) : ");
        }
        jeu.add(tmp);
        return jeu;
    }

    /**
     * Test method for the start game to be done. Test of affichageTas and jeuDeDepart.
     */
    
    void testAffichageJeuDeDepart(){

        System.out.println ("\n *** testJeuDeDepart()***");
        ArrayList<Integer> jeu = new ArrayList<Integer>();
        
        for (int i = 0; i <= 5 ; i++){
            jeu = jeuDeDepart();
            System.out.println(jeu);
            affichageTas(jeu);
            jeu.clear();
        }
    }

    /**
     * Change of player
     * @param joueurActuel having played
     * @param joueur1
     * @param joueur2
     * @return player to play
     */
    String changementJoueur (String joueurActuel, String joueur1, String joueur2){
        if ( joueurActuel == joueur1){
            joueurActuel = joueur2;
        }else {
            joueurActuel = joueur1;
        }
        return joueurActuel;
    }
    
    /** Player Change Test Method
     */
     void testchangementJoueur(){
         System.out.println ();
         System.out.println ("*** testchangementJoueur()");
         testCasChangementJoueur("alan","alan","louis","louis");
         testCasChangementJoueur("louis","alan","louis","alan");
    }
    
    /*** test a call for Changement Joueur
    * @param joueurActuel
    * @param joueur1
    * @param joueur2
    * @param result the expected result
    */
    void testCasChangementJoueur (String joueurActuel, String joueur1,
                                  String joueur2,String result) {
        
        // Arrange
        System.out.print ("changementJoueur (" + joueurActuel +", "+ joueur1 +
            ", "+ joueur2 +")");
		// Act
        String resExec = changementJoueur ( joueurActuel,joueur1, joueur2);	
        // Assert			 
        if (resExec == result){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }

    /**
     * Method for running a game
     */
    void partie(){
        int ligne, nbAllumASeparer;

        ArrayList<Integer> jeu = new ArrayList<Integer> ();
        jeu = jeuDeDepart();
        
        String joueur1 = SimpleInput.getString(" Entrez le nom du joueur 1 :");
        String joueur2 = "Ordinateur";
        String joueurActuel = joueur1;
        affichageTas(jeu);
        boolean possible, IAGagnante;
        do { 
            if(joueurActuel.equals(joueur1)){ // Tour du joueur 
                System.out.println(" Tour de " + joueurActuel);
                tourJoueur(jeu);
                
            }
            else{
                System.out.println(" Tour de " + joueurActuel);
                tourIA(jeu);
                
            }
            joueurActuel = changementJoueur(joueurActuel, joueur1, joueur2);
            //System.out.println("Le joueur actuel est " + joueurActuel);
            affichageTas(jeu);
            possible = estPossible(jeu);
        } while (possible);
            
        if ( joueurActuel == joueur1 ){
            // At the end of the winning player's turn, the current player changes
            // So the winning player is not the current player
            System.out.println(" Bravo à " + joueur2 +" qui remporte la partie !");
        } else{
            System.out.println(" Bravo à " + joueur1 +" qui remporte la partie !");
        }
    }

    /**
     * Procedure for the computer tour
     * @param jeu collection of items
     */
    void tourIA(ArrayList<Integer> jeu) {
        boolean IAGagnante;
        int ligne, nbAllumASeparer;
        IAGagnante = jouerGagnant(jeu);
        System.out.println("IAGagnante"+IAGagnante);
        if (!IAGagnante){
            do{
                ligne = (int) (Math.random() * jeu.size());
                System.out.println("La ligne " + ligne );
            } while ( (ligne > (jeu.size()-1) || ligne < 0) || (jeu.get(ligne) < 2) );
            //System.out.println("La ligne " + ligne );
            do{
                nbAllumASeparer = (int) (Math.random() * jeu.get(ligne));
            } while ( nbAllumASeparer >= jeu.get(ligne) || nbAllumASeparer <= 0 || ((nbAllumASeparer*2) == jeu.get(ligne)) );
            //System.out.println("nombre Allumettes"+ nbAllumASeparer);
            enlever(jeu,ligne, nbAllumASeparer);
        } else{
            jouerGagnant(jeu);
        }            
    }
    /**
     * Methode de test de TourIA
     * 
     */
    void testTourIA (){
        System.out.println();
        System.out.println("***testTourIA()***");
        ArrayList<Integer> jeu = new ArrayList<Integer> ();
        jeu.add(6);
        tourIA(jeu);
        affichageTas(jeu);
        System.out.println("Doit afficher une ligne de 4 et une de 2");
        jeu.add(3);
        tourIA(jeu);
        affichageTas(jeu);


    }
    
    
    /**
     * Progress of the player's turn
     * @param jeu collection of items
     */
    void tourJoueur (ArrayList<Integer> jeu){
        int ligne, nbAllumASeparer;
        
        if ( jeu.size() > 1 ) {
            ligne = SimpleInput.getInt("Entrez la ligne sur laquelle vous voulez jouer :");

            while ( (ligne > (jeu.size()-1) || ligne < 0) || (jeu.get(ligne) < 2) ){ 
                ligne = SimpleInput.getInt("Entrez la ligne sur laquelle vous voulez jouer :");
            }
        } else {
             ligne = 0; 
        }
        do {
            nbAllumASeparer = SimpleInput.getInt("Entrez le nombre de batons que vous voulez enlever de la ligne " + ligne + " :");
            //Condition sortie nbAllumASeparer < jeu.get(ligne) && nbAllumASeparer > 0 && nbAllumASeparer*2 != jeu.get(ligne)
        }while ( nbAllumASeparer >= jeu.get(ligne) || nbAllumASeparer <= 0 || ((nbAllumASeparer*2) == jeu.get(ligne)) );
        enlever(jeu,ligne, nbAllumASeparer);
    }
    /**
     * TourJoueur's test method
     */
    void testTourJoueur() {
        
        ArrayList<Integer> test = new ArrayList<Integer>();
        test.add(14);
        test.add(2);
        test.add(1);

        System.out.println("\n*** Test de la méthode tourJoueur() ***");
        System.out.println("CAS NORMAUX");
;       
        affichageTas(test);

        for (int i = 0 ; i < 5 ; i++ ) {
            tourJoueur(test);
            affichageTas(test);
        }
    
    }

     /**
     * Play the winning move if it exists
     * @param jeu collection of items
     * @return true if there is a winning move, false otherwise
     */
    boolean jouerGagnant(ArrayList<Integer> jeu) {
        boolean gagnant = false;;
        if (jeu == null) {
            System.err.println("suivant(): le paramètre jeu est null");
        } else {
            ArrayList<Integer> essai = new ArrayList<Integer>();
            int ligne = premier(jeu, essai);
			// implementation of rule number 2
			// A situation (or position) is said to be winning for the machine (or the player, whatever), if there is AT LEAST ONE 
			// decomposition (i.e. ONE action which consists in breaking a pile into 2 unequal piles) which is losing for the opponent.
            while (ligne != -1 && !gagnant) {
                if (estPerdante(essai)) {
                    jeu.clear();
                    gagnant = true;
                    for (int i = 0; i < essai.size(); i++) {
                        jeu.add(essai.get(i));
                    }
                } else {
                    ligne = suivant(jeu, essai, ligne);
                }

            }
        }
		
        return gagnant;
    }
	
	/**
     * RECURSIVE method which indicates if the configuration (of the current game or test game) is losing
     * 
     * @param jeu current game board (the state of the game at a certain time during the game)
     * @return true if the (game) configuration is losing, false otherwise
     */
    boolean estPerdante(ArrayList<Integer> jeu) {
	
        boolean ret = true; // by default the configuration is losing
		
        if (jeu == null) {
            System.err.println("estPerdante(): le paramètre jeu est null");
        }
		
		else {
		
			// if there are only piles of 1 or 2 matches left on the board
			// then the situation is necessarily losing (ret=true) = END of recursion
            if ( !estPossible(jeu) ) {
                ret = true;
            }
			
			// if the game board (passed as a parameter) is KNOWN to be LOSING
			// then we immediately return "true" and we stop the recursion (so we save time!)
			else if ( estConnuePerdante ( jeu ) ) {
				ret = true;
			}
			
			// otherwise you have to continue decomposing the heaps by creating the following test set
			else {
			
				// creation of a test set that will examine all possible decompositions
				// possible decompositions from the game
                ArrayList<Integer> essai = new ArrayList<Integer>(); // size = 0
				
				// creation of a test set that will examine all possible decompositions
				// possible decompositions from the game
                int ligne = premier(jeu, essai);
				
                while ( (ligne != -1) && ret) {
					// implementation of rule number 1
					// A situation (or position) is said to be losing for the machine (or the player, whatever) if and only if ALL 
					// its possible decompositions (i.e. ALL the actions which consist in decomposing a pile into 2 unequal piles) are 
					// ALL win for the opponent.
					// If ONLY ONE decomposition (from the game) is losing (for the opponent) then the configuration is NOT losing.
					// Here the call to "isLosing" is RECURRENT.
                    if (estPerdante(essai) == true) {					
                        ret = false;
						
                    } else {
						// generates the following test configuration (i.e. ONE possible decomposition)
						// from the set, if line = -1 there is no more possible decomposition
                        ligne = suivant(jeu, essai, ligne);
                    }
                }
				
				// if the "essai" configuration is losing (e.g. [4] or [3, 3] or [3, 3, 4]) then it is 
				// added to the table of losing positions
				if ( ret ) {
                    essai = normalise(essai);
                    posPerdantes.add ( essai );
                }
            }
        }
        cpt++;
        //System.out.println("PosPerdante"+posPerdantes.toString());
        return ret;
    }
 
	
	/**
     * Indicates whether the configuration is winning.
	 * Method that simply calls "isLosing".
     * 
     * @param jeu board
     * @return true if the configuration is winning, false otherwise
     */
    boolean estGagnante(ArrayList<Integer> jeu) {
        boolean ret = false;
        if (jeu == null) {
            System.err.println("estGagnante(): le paramètre jeu est null");
        } else {
            ret = !estPerdante(jeu);
        }
        return ret;
    }
    
    /**
     * Indicates the efficiency of estGagnante
     */
    void testEstGagnanteEfficacite() {
        
        System.out.println("\n");
        System.out.println("*** Test de l'efficacité de estGagnante() ***");
        // variables locales
        int n; 
        boolean indice;
        long t1, t2, diffT;
        ArrayList <Integer> eff = new ArrayList<Integer>();

        // initialisation
        n = 3;

        // 7 expériences
        for (int i = 1; i <= 30 ; i++){
            eff.add(n);
            //System.out.println("\nNombre d'allumettes n = " + n);

            cpt = 0;
            t1 = System.nanoTime();
            indice = estGagnante(eff);
            t2 = System.nanoTime();
            diffT = (t2 - t1); //in nanoseconds

            //System.out.println("Tps = " + diffT + " ns");
            //System.out.println("Après appel de la méthode, compteur cpt = " + cpt);
            System.out.println(n + " " + diffT + " " + cpt);

            n++;
            posPerdantes.clear();
            eff.clear(); 
        }
    }

    /**
     * Brief tests of the playerWinner() method
     */
    void testJouerGagnant() {
        System.out.println();
        System.out.println("*** testJouerGagnant() ***");

        System.out.println("Test des cas normaux");
        ArrayList<Integer> jeu1 = new ArrayList<Integer>();
        jeu1.add(6);
        ArrayList<Integer> resJeu1 = new ArrayList<Integer>();
        resJeu1.add(4);
        resJeu1.add(2);
		
        testCasJouerGagnant(jeu1, resJeu1, true);
        
    }

    /**
     * Testing a case of the playWinner() method
	 *
	 * @param jeu the game board
	 * @param resJeu the game board after playing win
	 * @param res the result expected by playWinner
     */
    void testCasJouerGagnant(ArrayList<Integer> jeu, ArrayList<Integer> resJeu, boolean res) {
        // Arrange
        System.out.print("jouerGagnant (" + jeu.toString() + ") : ");

        // Act
        boolean resExec = jouerGagnant(jeu);

        // Assert
        System.out.print(jeu.toString() + " " + resExec + " : ");
        if (jeu.equals(resJeu) && res == resExec) {
            System.out.println("OK\n");
        } else {
            System.err.println("ERREUR\n");
        }
    }	

    /**
     * Divide the matches in a line into two piles (1 line = 1 pile)
     * 
     * @param jeu array of matches per line
     * @param ligne line (heap) on which the matches must be separated
     * @param nb number of matches REMOVED from the line after separation
     */
    void enlever ( ArrayList<Integer> jeu, int ligne, int nb ) {
		// traitement des erreurs
        if (jeu == null) {
            System.err.println("enlever() : le paramètre jeu est null");
        } else if (ligne >= jeu.size()) {
            System.err.println("enlever() : le numéro de ligne est trop grand");
        } else if (nb >= jeu.get(ligne)) {
            System.err.println("enlever() : le nb d'allumettes à retirer est trop grand");
        } else if (nb <= 0) {
            System.err.println("enlever() : le nb d'allumettes à retirer est trop petit");
        } else if (2 * nb == jeu.get(ligne)) {
            System.err.println("enlever() : le nb d'allumettes à retirer est la moitié");
        } else {
			// new heap (box) added to the game (necessarily at the end of the table)
			// this new pile contains the number of matches removed (nb) from the pile to be separated			
            jeu.add(nb);
			// the remaining pile with "nb" matches less
            jeu.set(ligne, jeu.get(ligne) - nb);
        }
    }

    /**
     * Test if it is possible to separate one of the piles
     * 
     * @param jeu board
     * @return true if there is at least one pile of 3 or more matches, false otherwise
     */
    boolean estPossible(ArrayList<Integer> jeu) {
        boolean ret = false;
        if (jeu == null) {
            System.err.println("estPossible(): le paramètre jeu est null");
        } else {
            int i = 0;
            while (i < jeu.size() && !ret) {
                if (jeu.get(i) > 2) {
                    ret = true;
                }
                i = i + 1;
            }
        }
        return ret;
    }

    /**
     * Create a very first test configuration from the game
     * 
     * @param jeu board
     * @param jeuEssai new game configuration
     * @return the number of the pile divided in two or (-1) if there is no pile of at least 3 matches
     */
    int premier(ArrayList<Integer> jeu, ArrayList<Integer> jeuEssai) {
	
        int numTas = -1; // no heap to separate by default
		int i;
		
        if (jeu == null) {
            System.err.println("premier(): le paramètre jeu est null");
        } else if (!estPossible((jeu)) ){
            System.err.println("premier(): aucun tas n'est divisible");
        } else if (jeuEssai == null) {
            System.err.println("estPossible(): le paramètre jeuEssai est null");
        } else {
            // before copying the game to TestGame there is a TestGame reset 
            jeuEssai.clear();
            i = 0;
			
			// copy square by square
			// testgame is the same as the original game
            while (i < jeu.size()) {
                jeuEssai.add(jeu.get(i));
                i = i + 1;
            }
			
            i = 0;
			// search for a pile of matches of at least 3 matches in the game
			// otherwise numTas = -1
			boolean trouve = false;
            while ( (i < jeu.size()) && !trouve) {
				
				// if you find a pile of at least 3 matches
				if ( jeuEssai.get(i) >= 3 ) {
					trouve = true;
					numTas = i;
				}
				
				i = i + 1;
            }
			
			// separates the pile (numTas box) into a pile of ONE match at the end of the table 
			// the pile in the numTas box has decreased by one match (removal of one match)
			// testGame is the game board that shows this separation
            if ( numTas != -1 ) enlever ( jeuEssai, numTas, 1 );
        }
		
        return numTas;
    }

    /**
     * Brief tests of the premier() method
     */
    void testPremier() {
        System.out.println();
        System.out.println("*** testPremier()");

        ArrayList<Integer> jeu1 = new ArrayList<Integer>();
        jeu1.add(10);
        jeu1.add(11);
        int ligne1 = 0;
        ArrayList<Integer> res1 = new ArrayList<Integer>();
        res1.add(9);
        res1.add(11);
        res1.add(1);
        testCasPremier(jeu1, ligne1, res1);
    }

    /**
     * Test a case of the testFirst method
	 * @param jeu the game board
	 * @param ligne the number of the heap separated first
	 * @param res the game board after a first split
     */
    void testCasPremier(ArrayList<Integer> jeu, int ligne, ArrayList<Integer> res) {
        // Arrange
        System.out.print("premier (" + jeu.toString() + ") : ");
        ArrayList<Integer> jeuEssai = new ArrayList<Integer>();
        // Act
        int noLigne = premier(jeu, jeuEssai);
        // Assert
        System.out.println("\nnoLigne = " + noLigne + " jeuEssai = " + jeuEssai.toString());
        if (jeuEssai.equals(res) && noLigne == ligne) {
            System.out.println("OK\n");
        } else {
            System.err.println("ERREUR\n");
        }
    }

    /**
     * Generates the following test configuration (i.e. ONE possible decomposition)
     * 
     * @param jeu game board
     * @param jeuEssai configuration of the game after splitting
     * @param ligne the number of the heap that was last split
     * @return the number of the heap divided in two for the new configuration, -1 if no more decomposition is possible
     */
    int suivant(ArrayList<Integer> jeu, ArrayList<Integer> jeuEssai, int ligne) {
	
        // System.out.println("suivant(" + jeu.toString() + ", " +jeuEssai.toString() +
        // ", " + ligne + ") = ");
		
		int numTas = -1; // by default there is no more decomposition possible
		
        int i = 0;
		// error handling
        if (jeu == null) {
            System.err.println("suivant(): le paramètre jeu est null");
        } else if (jeuEssai == null) {
            System.err.println("suivant() : le paramètre jeuEssai est null");
        } else if (ligne >= jeu.size()) {
            System.err.println("estPossible(): le paramètre ligne est trop grand");
        }
		
		else {
		
			int nbAllumEnLigne = jeuEssai.get(ligne);
			int nbAllDernCase = jeuEssai.get(jeuEssai.size() - 1);
			
			// if on the same line (passed in parameter) we can still remove matches,
			// i.e. if the difference between the number of matches on this line and
			// the number of matches at the end of the array is > 2, then we remove another
			// 1 match on this line and add 1 match at the last cell		
            if ( (nbAllumEnLigne - nbAllDernCase) > 2 ) {
                jeuEssai.set ( ligne, (nbAllumEnLigne - 1) );
                jeuEssai.set ( jeuEssai.size() - 1, (nbAllDernCase + 1) );
                numTas = ligne;
            } 
			
			// if not, the next heap (line) in the game must be examined for possible breakdown
			// a new test configuration is recreated, identical to the game board
			else {
                // copy of jeu in JeuEssai
                jeuEssai.clear();
                for (i = 0; i < jeu.size(); i++) {
                    jeuEssai.add(jeu.get(i));
                }
				
                boolean separation = false;
                i = ligne + 1; // tas suivant
				// if there is still a pile and it contains at least 3 matches
				// then a first separation is made by removing 1 match
                while ( i < jeuEssai.size() && !separation ) {
					// the pile must be at least 3 matches long
                    if ( jeu.get(i) > 2 ) {
                        separation = true;
						// we start by removing 1 match from this pile
                        enlever(jeuEssai, i, 1);
						numTas = i;
                    } else {
                        i = i + 1;
                    }
                }				
            }
        }
		
        return numTas;
    }

    /**
     * Brief tests of the following() method
     */
    void testSuivant() {
        System.out.println();
        System.out.println("*** testSuivant() ****");

        int ligne1 = 0;
        int resLigne1 = 0;
        ArrayList<Integer> jeu1 = new ArrayList<Integer>();
        jeu1.add(10);
        ArrayList<Integer> jeuEssai1 = new ArrayList<Integer>();
        jeuEssai1.add(9);
        jeuEssai1.add(1);
        ArrayList<Integer> res1 = new ArrayList<Integer>();
        res1.add(8);
        res1.add(2);
        testCasSuivant(jeu1, jeuEssai1, ligne1, res1, resLigne1);

        int ligne2 = 0;
        int resLigne2 = -1;
        ArrayList<Integer> jeu2 = new ArrayList<Integer>();
        jeu2.add(10);
        ArrayList<Integer> jeuEssai2 = new ArrayList<Integer>();
        jeuEssai2.add(6);
        jeuEssai2.add(4);
        ArrayList<Integer> res2 = new ArrayList<Integer>();
        res2.add(10);
        testCasSuivant(jeu2, jeuEssai2, ligne2, res2, resLigne2);

        int ligne3 = 1;
        int resLigne3 = 1;
        ArrayList<Integer> jeu3 = new ArrayList<Integer>();
        jeu3.add(4);
        jeu3.add(6);
        jeu3.add(3);
        ArrayList<Integer> jeuEssai3 = new ArrayList<Integer>();
        jeuEssai3.add(4);
        jeuEssai3.add(5);
        jeuEssai3.add(3);
        jeuEssai3.add(1);
        ArrayList<Integer> res3 = new ArrayList<Integer>();
        res3.add(4);
        res3.add(4);
        res3.add(3);
        res3.add(2);
        testCasSuivant(jeu3, jeuEssai3, ligne3, res3, resLigne3);

    }

    /**
     * Test a case of the following method
	 * @param jeu the game board
	 * @param jeuEssai the game board obtained after splitting a heap
	 * @param ligne is the number of the heap that was last split
	 * @param resJeu is the expected test game after splitting
	 * @param resLigne is the expected number of the heap that is split
     */
    void testCasSuivant(ArrayList<Integer> jeu, ArrayList<Integer> jeuEssai, int ligne, ArrayList<Integer> resJeu, int resLigne) {
        // Arrange
        System.out.print("suivant (" + jeu.toString() + ", " + jeuEssai.toString() + ", " + ligne + ") : ");
        // Act
        int noLigne = suivant(jeu, jeuEssai, ligne);
        // Assert
        System.out.println("\nnoLigne = " + noLigne + " jeuEssai = " + jeuEssai.toString());
        if (jeuEssai.equals(resJeu) && noLigne == resLigne) {
            System.out.println("OK\n");
        } else {
            System.err.println("ERREUR\n");
        }
    }
	
    /**
     * Détermine si la configuration est connue comme perdante dans posPerdantes
     * 
     * @param jeu le plateau de jeu
     * @return vrai si le jeu est dans posPerdantes
     */
    boolean estConnuePerdante ( ArrayList<Integer> jeu ) {
        // création d'une copie de jeu triée sans 1, ni 2
        ArrayList<Integer> copie = normalise(jeu);
        boolean ret = false;
        int i = 0;
        while ( !ret && i < posPerdantes.size() ) {
            if (sontIdentiques(copie, posPerdantes.get(i))) {
                ret = true;
            }
            i++;
        }
        return ret;
    }

    ArrayList<Integer> normalise(ArrayList<Integer> jeu){
        ArrayList<Integer> normal = new ArrayList<Integer>();
        for(int i = 0;i<jeu.size();i++){
            if(jeu.get(i)>2){
                normal.add(jeu.get(i));
            }
        }
        Collections.sort(normal);
        return normal;
    }

    /**
     * Normalise's test Method
     * 
	*/
    void testNormalise(){
        System.out.println("\n *** testNormalise() ***");
        ArrayList<Integer> jeu = new ArrayList<Integer>();
        ArrayList<Integer> result = new ArrayList<Integer>();
        jeu.add(9);
        jeu.add(1);
        jeu.add(2);
        jeu.add(3);
        result.add(3);
        result.add(9);
        testCasNormalise(jeu, result);
        jeu.clear();
        result.clear();
        jeu.add(9);
        jeu.add(1);
        jeu.add(8);
        jeu.add(1);
        result.add(8);
        result.add(9);
        testCasNormalise(jeu, result);
    }
    /**
     * 
     * Test a case of the normalise's test method
     * 
     * @param jeu collection of items
     * @param result result of the call of jeu by normalise
     */
    void testCasNormalise(ArrayList<Integer> jeu, ArrayList<Integer> result){
        // Arrange
        System.out.println ("Normalise (" + jeu.toString() +", resultat attendu : "+ result.toString()+")");
		// Act
        ArrayList<Integer> resExec = new ArrayList<Integer>();
        resExec = normalise(jeu);
        System.out.println("Résultat : " + resExec.toString());
        // Assert			 
        if (resExec.equals(result)){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }

    /**
     * Checks if 2 ArrayList<Integer> are identical 2 by 2
     * @param copie 
     * @param info
     * @return true if the 2 ArrayList entered as parameters are identical, false otherwise
     */
    boolean sontIdentiques(ArrayList<Integer> copie, ArrayList<Integer> info){
        boolean ret = false;
        int i = 0;
        int j = 0;
        ArrayList<Integer> test = new ArrayList<>(info);
        if(copie.size() == test.size()){ // 3-3-4 posper 4-3-5 copie
            do{
                ret = false;
                j = 0;
                while(j < test.size() && ret == false){
                    if(copie.get(i) == test.get(j)){
                        ret = true;
                        test.remove(j);
                    } else {
                        j++;
                    }
                }
                i++;
            } while(i < copie.size() && ret);
        }

        return ret;
    }
    /**
     * SontIdentiques's test method
     */
    void testSontIdentiques(){
        System.out.println("\n *** testSontIdentiques() ***");
        ArrayList<Integer> copie = new ArrayList<Integer>();
        ArrayList<Integer> positionsA= new ArrayList<Integer>();
        ArrayList<Integer> positionsB= new ArrayList<Integer>();

        copie.add(9);
        copie.add(3);
        copie.add(5);
        copie.add(4);
        positionsA.add(10);
        positionsA.add(4);
        positionsA.add(2);
        positionsA.add(7);

        positionsB.add(7);
        positionsB.add(10);
        positionsB.add(4);
        positionsB.add(2);

        testCasSontIdentiques(copie,copie,true);
        testCasSontIdentiques(copie,positionsA,false);
        testCasSontIdentiques(copie,positionsB,false);
        testCasSontIdentiques(positionsA,positionsB,true);
        testCasSontIdentiques(positionsB, positionsB, true);

    }
     /**
     * Test a case of the sontIdentiques's test method
     * @param copie collection of items
     * @param positionsPerdantes collection of loosing positions
     * @param ret result of the call of copie and positionsPerdantes by sontIdentiques
     */
    void testCasSontIdentiques(ArrayList<Integer> copie, ArrayList<Integer> positionsPerdantes,boolean ret){
        // Arrange
        System.out.print ("ArrayList <Integer> copie : " + copie.toString() +" ArrayList<Integer> positionsPerdantes : " + positionsPerdantes.toString());
		// Act
        boolean resExec;
        resExec = sontIdentiques(copie,positionsPerdantes);
        // Assert			 
        if ( resExec == ret ){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }
}



