import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


/**
 * @author Dewi Monboussin
 * Grundy2 game
 * Test efficiency of estGagnante method
 */

class DEWI3 {
	long cpt;
	// list of all losing situations 
	ArrayList<ArrayList<Integer>> sitPerdantes = new ArrayList<ArrayList<Integer>>();
	// list of all winning situations
	ArrayList<ArrayList<Integer>> sitGagnantes = new ArrayList<ArrayList<Integer>>();
	
	/**
	 * Main method of class
	 */
	void principal(){
		//testEstConnuePerdante()
		//testIdentiques();
		//testNormalise();
		//testEstConnueGagnante()
		//testSimplification();
		testEstGagnanteEfficacite();
	}
	
	
	
	/**
	 * Method allowing to start a game 
	 */
	void partie (){
		String J1 = SimpleInput.getString("Nom du joueur : ");
		// if player = false so it's turn of IA
		boolean joueur = true;
		boolean fin = false;
		int allumettes = SimpleInput.getInt("Nombre d'allumettes choisies : ");
		int c;
		int nb;
		int nbCases = 1;
		ArrayList<Integer>jeu = new ArrayList<Integer>();
		jeu.add(allumettes);
		System.out.println(jeu.toString());
		
		while (!fin) {
			if ( joueur ) {
				System.out.println("J1 c'est votre tour ");
				c = choixCase(jeu);
				nb = SimpleInput.getInt("Nombre d'allumettes à séparer : ");
				enlever(jeu,c,nb);
				System.out.println(jeu.toString());
				joueur = false;
			}else{
				System.out.println("Machine c'est à vous de jouer");
				if (!jouerGagnant(jeu)){
					c = (int) (Math.random()*nbCases);
					while ( c > jeu.size() || c < 0 || jeu.get(c) < 3){
						c = (int) (Math.random()*nbCases);
					}
					nbCases++;
					int max  = jeu.get(c);
					nb = (int) (Math.random()*(max));
					while ( nb == jeu.get(c)/2 || nb > jeu.get(c) || nb <= 0){
						nb = (int) (Math.random()*(max));
					}
					enlever(jeu,c,nb);
				}
				System.out.println(jeu.toString());
				joueur = true;
			}
			fin = conditionFin(jeu);
		}
		
		if (joueur){
			System.out.println("Machine vous avez gagné");
		}else{
			System.out.println("J1 vous avez gagné");
		}			
	}
	
	
    /**
     * Play the winning move if this is possible
     * @param jeu collection of items
     * @return true if there is a winning move, false otherwise
     */
	boolean jouerGagnant(ArrayList<Integer> jeu) {
        boolean gagnant = false;
        if (jeu == null) {
            System.err.println("suivant(): le paramètre jeu est null");
        } else {
            ArrayList<Integer> essai = new ArrayList<Integer>();
            int ligne = premier(jeu, essai);
			// Implementation of rule number 2
            // A situation is winning for IA (or player), if there is at least one decompostion losing for opponent
            while (ligne != -1 && !gagnant) {
                if (estPerdante(essai)) {
                    jeu.clear();
                    gagnant = true;
                    for (int i = 0; i < essai.size(); i++) {
                        jeu.add(essai.get(i));
                    }
                } else {
                    ligne = suivant(jeu, essai, ligne);
                }
            }
        }
		
        return gagnant;
    }
    
    /**
     * Few tests of jouerGagnant method
     */
    void testJouerGagnant() {
        System.out.println();
        System.out.println("*** testJouerGagnant() ***");

        System.out.println("Test des cas normaux");
        ArrayList<Integer> jeu1 = new ArrayList<Integer>();
        jeu1.add(6);
        ArrayList<Integer> resJeu1 = new ArrayList<Integer>();
        resJeu1.add(4);
        resJeu1.add(2);
		
        testCasJouerGagnant(jeu1, resJeu1, true);
        
    }

    /**
     * Methode allowing to test cases of jouerGagnant method
	 *
	 * @param jeu game board
	 * @param resJeu game board after jouerGagnant
	 * @param res the result expected after jouerGagnant
     */
    void testCasJouerGagnant(ArrayList<Integer> jeu, ArrayList<Integer> resJeu, boolean res) {
        // Arrange
        System.out.print("jouerGagnant (" + jeu.toString() + ") : ");

        // Act
        boolean resExec = jouerGagnant(jeu);

        // Assert
        System.out.print(jeu.toString() + " " + resExec + " : ");
        if (jeu.equals(resJeu) && res == resExec) {
            System.out.println("OK\n");
        } else {
            System.err.println("ERREUR\n");
        }
    }
    
    
    
    /**
     * Recursive methode which noted if the configuration is losing
     * 
     * @param jeu currently collection of items
     * @return true if game configuration is losing, false otherwise
     */
    boolean estPerdante(ArrayList<Integer> jeu) {
        boolean ret = true; // by default configuration is losing
        if (jeu == null) {
            System.err.println("estPerdante(): le paramètre jeu est null");
        }
		else {
			
			jeu = simplification(jeu);
			// if there are only stacks of 1 or 2 matches in the game
			// so situation is obviously losing (ret = true)= end of recursion
            if ( !estPossible(jeu) ) {
                ret = true;
                // if the game is known from the list ret = true, end of recursion
            }else if ( estConnuePerdante(jeu)){
				// remove items of game if they is know from the losing list
				ret = true;
				// if the game is known from the winning list ret= true, and of recursion
			}else if (estConnueGagnante(jeu)){
				ret = false; 
			}
			
			// otherwise decomposition continue
			else {
				// first decomposition : remove 1 match at the first stack wich have
				// at least 3 matches, ligne = -1 means there are no more stack of 3 matches at least
                ArrayList<Integer> essai = new ArrayList<Integer>(); // size = 0
				
				// first decomposition : remove 1 match at the first stack wich have
				// at least 3 matches, ligne = -1 means there are no more stack of 3 matches at least
                int ligne = premier(jeu, essai);
                while ( (ligne != -1) && ret) {
					// Implementation of rule number 1
					// a situation is losing fort the IA(or player) if all decompositions availabe are all winning for the opponent
					// if only one decomposition is losing so the configuration is not losing
					// here estPerdante is recursive
                    if (estPerdante(essai) == true) {
                        ret = false;
						
                    } else {
						// generate the next try configuration from the game if ligne = -1 there is no more available decomposition
                        ligne = suivant(jeu, essai, ligne);
                    }
                }
                // if trying configuration is losing and unknown from the losing list we add it inside
                if (ret){
					essai = normalise(jeu);
					sitPerdantes.add(essai);
					// if trying configuration is winning et unknown from the winning list we add it inside 	
				}else{
					essai = normalise(jeu);
					sitGagnantes.add(essai);
				}
            }
        }
        cpt++;
        return ret;
    }
	
	/**
	 * Indicate if configuration is winning
	 * Methode which only call estPerdante
     * 
     * @param jeu plateau de jeu
     * @return vrai si la configuration est gagnante, faux sinon
     */
    boolean estGagnante(ArrayList<Integer> jeu) {
        boolean ret = false;
        if (jeu == null) {
            System.err.println("estGagnante(): le paramètre jeu est null");
        } else {
            ret = !estPerdante(jeu);
        }
        return ret;
    }
    
    /**
     * Divided in two stack matches of a line of game
     * 
     * @param jeu   matches board by line 
     * @param ligne stack in wich matches must be separated 
     * @param nb  number of remove matches of line after separation
     */
    void enlever ( ArrayList<Integer> jeu, int ligne, int nb ) {
		// errors handling 
        if (jeu == null) {
            System.err.println("enlever() : le paramètre jeu est null");
        } else if (ligne >= jeu.size()) {
            System.err.println("enlever() : le numéro de ligne est trop grand");
        } else if (nb >= jeu.get(ligne)) {
            System.err.println("enlever() : le nb d'allumettes à retirer est trop grand");
        } else if (nb <= 0) {
            System.err.println("enlever() : le nb d'allumettes à retirer est trop petit");
        } else if (2 * nb == jeu.get(ligne)) {
            System.err.println("enlever() : le nb d'allumettes à retirer est la moitié");
        } else {
			// new stack add to the game
			// this new stack contain the number of removing matches of separated stack			
            jeu.add(nb);
            // remaining stack with number of less matches
            jeu.set(ligne, jeu.get(ligne) - nb);
        }
    }

    /**
     * Test if is possible de separate one of  the stacks
     * 
     * @param jeu collection of items
     * @return true if there is at least a stack of 3 matches, false otherwise
     */
    boolean estPossible(ArrayList<Integer> jeu) {
        boolean ret = false;
        
        if (jeu == null) {
            System.err.println("estPossible(): le paramètre jeu est null");
        } else {
            int i = 0;
            while (i < jeu.size() && !ret) { 
                if (jeu.get(i) > 2) {
                    ret = true;
                }
                i = i + 1;
            }
        }
        return ret;
    }

    /**
     * Create a first trying configuration from the game
     * 
     * @param jeu collections of items
     * @param jeuEssai new game configuration
     * @return number of stack divided in two or (-1) if there is no more stack of 3 matches or more
     */
    int premier(ArrayList<Integer> jeu, ArrayList<Integer> jeuEssai) {
	
        int numTas = -1; // By defautl, no stack to divided
		int i;
		
        if (jeu == null) {
            System.err.println("premier(): le paramètre jeu est null");
        } else if (!estPossible((jeu)) ){
            System.err.println("premier(): aucun tas n'est divisible");
        } else if (jeuEssai == null) {
            System.err.println("estPossible(): le paramètre jeuEssai est null");
        } else {
			// before the copy of game in jeuEssaithere is a reset of jeuEssai
            jeuEssai.clear();
            i = 0;
			
			// copy box by box
			// jeuEssai is the same that the starting game
            while (i < jeu.size()) {
                jeuEssai.add(jeu.get(i));
                i = i + 1;
            }
			
            i = 0;
            // search a stack of 3 matches or more in the game
			// otherwise numTas = -1
			boolean trouve = false;
            while ( (i < jeu.size()) && !trouve) {
				
				// if we find a stack of 3 matches at least
				if ( jeuEssai.get(i) >= 3 ) {
					trouve = true;
					numTas = i;
				}
				
				i = i + 1;
            }
			
			// cut the stack in a only one match stack at the end of board
			// the stack in numTas box is decreased from one match
			// jeuEssai is the game board which make appear this separation
            if ( numTas != -1 ) enlever ( jeuEssai, numTas, 1 );
        }
		
        return numTas;
    }
    
    /**
     * Generate the next trying configuration
     * 
     * @param jeu game board
     * @param jeuEssai trying configuration of game after separation
     * @param ligne number of stack which is the last to be separated 
     * @return number of divided stack in two for the new configuration -1 if no more available decomposition 
     */
	int suivant(ArrayList<Integer> jeu, ArrayList<Integer> jeuEssai, int ligne) {
	
        // System.out.println("suivant(" + jeu.toString() + ", " +jeuEssai.toString() +
        // ", " + ligne + ") = ");
		
		int numTas = -1; // by default, there is no more available decomposition
		
        int i = 0;
		// error handling
        if (jeu == null) {
            System.err.println("suivant(): le paramètre jeu est null");
        } else if (jeuEssai == null) {
            System.err.println("suivant() : le paramètre jeuEssai est null");
        } else if (ligne >= jeu.size()) {
            System.err.println("estPossible(): le paramètre ligne est trop grand");
        }
		
		else {
		
			int nbAllumEnLigne = jeuEssai.get(ligne);
			int nbAllDernCase = jeuEssai.get(jeuEssai.size() - 1);
			
			// if if the same line, we can remove matches
			// means if the gap between number of matches on this line and 
			// number of matches at the end of board is > 2, so we remove
			// again one match on this line and we add 1 match at the last box		
            if ( (nbAllumEnLigne - nbAllDernCase) > 2 ) {
                jeuEssai.set ( ligne, (nbAllumEnLigne - 1) );
                jeuEssai.set ( jeuEssai.size() - 1, (nbAllDernCase + 1) );
                numTas = ligne;
            } 
			
			// otherwis we must examine next stack to potentially decompose it 
			// we recreate a new trying configuration identical to game board
			else {
                // copy of game in jeuEssai
                jeuEssai.clear();
                for (i = 0; i < jeu.size(); i++) {
                    jeuEssai.add(jeu.get(i));
                }
				
                boolean separation = false;
                i = ligne + 1; // next stack
                // if there is a stack which contains at least 3 matches
                // so we do a first separation to remove 1 match
                while ( i < jeuEssai.size() && !separation ) {
					// stack size must be 3 matches minimum
                    if ( jeu.get(i) > 2 ) {
                        separation = true;
                        // we start by removing 1 match in this stack
                        enlever(jeuEssai, i, 1);
						numTas = i;
                    } else {
                        i = i + 1;
                    }
                }				
            }
        }
		
        return numTas;
    }
    
    
    /**
     * Few tests of the following() method
     */
    void testSuivant() {
        System.out.println();
        System.out.println("*** testSuivant() ****");

        int ligne1 = 0;
        int resLigne1 = 0;
        ArrayList<Integer> jeu1 = new ArrayList<Integer>();
        jeu1.add(10);
        ArrayList<Integer> jeuEssai1 = new ArrayList<Integer>();
        jeuEssai1.add(9);
        jeuEssai1.add(1);
        ArrayList<Integer> res1 = new ArrayList<Integer>();
        res1.add(8);
        res1.add(2);
        testCasSuivant(jeu1, jeuEssai1, ligne1, res1, resLigne1);

        int ligne2 = 0;
        int resLigne2 = -1;
        ArrayList<Integer> jeu2 = new ArrayList<Integer>();
        jeu2.add(10);
        ArrayList<Integer> jeuEssai2 = new ArrayList<Integer>();
        jeuEssai2.add(6);
        jeuEssai2.add(4);
        ArrayList<Integer> res2 = new ArrayList<Integer>();
        res2.add(10);
        testCasSuivant(jeu2, jeuEssai2, ligne2, res2, resLigne2);

        int ligne3 = 1;
        int resLigne3 = 1;
        ArrayList<Integer> jeu3 = new ArrayList<Integer>();
        jeu3.add(4);
        jeu3.add(6);
        jeu3.add(3);
        ArrayList<Integer> jeuEssai3 = new ArrayList<Integer>();
        jeuEssai3.add(4);
        jeuEssai3.add(5);
        jeuEssai3.add(3);
        jeuEssai3.add(1);
        ArrayList<Integer> res3 = new ArrayList<Integer>();
        res3.add(4);
        res3.add(4);
        res3.add(3);
        res3.add(2);
        testCasSuivant(jeu3, jeuEssai3, ligne3, res3, resLigne3);

    }

    /**
     * Test a case of the following method
	 * 
	 * @param jeu game board
	 * @param jeuEssai game board obtained after splitting a stack
	 * @param ligne is the number of the stack that was last split
	 * @param resJeu is the expected results after splitting
	 * @param resLigne is the expected number of the stack that is split
     */
    void testCasSuivant(ArrayList<Integer> jeu, ArrayList<Integer> jeuEssai, int ligne, ArrayList<Integer> resJeu, int resLigne) {
        // Arrange
        System.out.print("suivant (" + jeu.toString() + ", " + jeuEssai.toString() + ", " + ligne + ") : ");
        // Act
        int noLigne = suivant(jeu, jeuEssai, ligne);
        // Assert
        System.out.println("\nnoLigne = " + noLigne + " jeuEssai = " + jeuEssai.toString());
        if (jeuEssai.equals(resJeu) && noLigne == resLigne) {
            System.out.println("OK\n");
        } else {
            System.err.println("ERREUR\n");
        }
    }
    
    
	/**
	 * Player choose the line in wich he will remove matches
	 * if the number is superior to the maximum line 
	 * method ask again an input
	 * 
	 * @param jeu boarding game
	 */  
	int  choixCase (ArrayList<Integer>jeu) {
		boolean correct = true;
		int c = SimpleInput.getInt ("case : ");
		while (c >= jeu.size() || c < 0 || jeu.get(c) < 3) {
			c = SimpleInput.getInt ("case : ");
		}
		return c;
	}
	
	/**
	 * method of ending conditions
	 * @param jeu boarding game
	 */
	boolean conditionFin (ArrayList<Integer>jeu) {
		int i = 0;
		boolean terminer = true;
		while (i < jeu.size() && terminer) {
			if (jeu.get(i) > 2) {
				terminer = false;
			}
			i++;
		}
		return terminer;
	}
	
	/**
	 * Methode allowing to determine if situation is already in sitPerdantes
	 * @param jeu game board
	 * @param return true if game is already in sitPerdantes
	 */
	boolean estConnuePerdante (ArrayList<Integer>jeu){
		// copy of game
		ArrayList<Integer>copie = normalise(jeu);
		boolean ret = false;
		int i = 0;
		while ( !ret && i < sitPerdantes.size()){
			if (identiques(copie,sitPerdantes.get(i))){
				ret = true;
			}
			i++;
		}
		return ret;
	}
	
	/**
     * Method allowing to test estConnuePerdante method
     */
    void testEstConnuePerdante() {
		System.out.println();
        System.out.println("*** testEstConnuePerdante : ");
        // Basic cases
        System.out.println("estConnuePerdante n°1 : ");
        ArrayList<Integer> c1 = new ArrayList<Integer>();
		c1.add(7);
        c1.add(10);
        c1.add(4);
        sitPerdantes.add(c1);
        testCasEstConnuePerdante(c1,true);
        sitPerdantes.clear();
        System.out.println("estConnuePerdante n°2 : ");
        ArrayList<Integer> c2 = new ArrayList<Integer>();
        c2.add(90);
        c2.add(40);
        c2.add(100);
        sitPerdantes.add(c1);
        testCasEstConnuePerdante(c2,false);
    }

    /**
     * Method allowing to test several cases of estConnuePerdante method
     * @param copie collection of integers items
     * @param ret boolean expected after method
     */
    void testCasEstConnuePerdante(ArrayList<Integer> copie,boolean ret ){
            // Arrange
            System.out.println ("copie de la liste : " + copie.toString()+ " Resultat attendu :  " + ret ) ;
            // Act
            boolean resExec;
            resExec = estConnuePerdante(copie);
            System.out.println("Liste des situations perdantes : " + sitPerdantes.toString());
            // Assert			 
            if ( resExec == ret ){
                System.out.println ("OK");
            } else {
                System.err.println ("ERREUR");
            }
    }
	
	
	
	
	/**
	 * Methode allowing to determine of a situation is already known
	 * in the winning list
	 * @param jeu game board
	 * @param return true if situation is known, false otherwise
	 */
	boolean estConnueGagnante (ArrayList<Integer>jeu){
		// copy of game
		ArrayList<Integer>copie = normalise(jeu);
		// by default situation is unknown 
		boolean ret = false;
		int i = 0;
		while ( !ret && i < sitGagnantes.size()){
			if (identiques(copie,sitGagnantes.get(i))){
				ret = true;
			}
			i++;
		}
		return ret;
	}
	
	/**
     * Method allowing to test estConnueGagnante
     */
    void testEstConnueGagnante() {
		System.out.println();
        System.out.println("*** test estConnueGagnante : ");
        // Basic cases
        System.out.println("estConnuGagnante n°1 : ");
        ArrayList<Integer> c1 = new ArrayList<Integer>();
        c1.add(4);
        c1.add(90);
        c1.add(1);
        c1.add(56);
        sitGagnantes.add(c1);
        testCasEstConnueGagnante(c1,true);
        sitGagnantes.clear();
        System.out.println("estConnueGagnante n°2 : ");
        ArrayList<Integer> c2 = new ArrayList<Integer>();
        c2.add(90);
        c2.add(60);
        sitGagnantes.add(c1);
        testCasEstConnueGagnante(c2,false);
    }

    /**
     * Allows to test several cases of estConnueGagnante
     * @param copie collection of items
     * @param ret boolean expected after estConnueGagnante
     */
    void testCasEstConnueGagnante(ArrayList<Integer> copie,boolean ret ){
		// Arrange
		System.out.println ("copie de la liste : " + copie.toString()+ " Resultat attendu :  "+ ret );
		// Act
		boolean resExec;
		resExec = estConnueGagnante(copie);
		System.out.println("Liste de situations gagnantes : " + sitGagnantes.toString());
		// Assert			 
		if ( resExec == ret ){
			System.out.println ("OK");
		} else {
			System.err.println ("ERREUR");
		}
    }
	
	
		
	/**
	 * Method allowing do determine if two lists contan the same items
	 * @param l1 first list
	 * @param l2 second list
	 * @return true if lists are identical, false otherwise
	 */
	boolean identiques (ArrayList<Integer>l1,ArrayList<Integer>l2){
		int i = 0;
		int j;
		int present = 0;
		// by default lists are not identical
		boolean ret = false;
		
		if (l1.size() == l2.size()){
			Collections.sort(l1);
			Collections.sort(l2);
			ret = true;
			i = 0;
			while(ret && i < l1.size()){
				if (l1.get(i) != l2.get(i)){
					ret = false;
				}
				i++;
			}
		}
		return ret;
	}
	
	/**
     * Allows to test identiques
     */
    void testIdentiques(){
		System.out.println();
        System.out.println("*** testIdentiques : ");
        //Basic cases
        System.out.println("identiques n°1 : ");
        ArrayList<Integer> c1 = new ArrayList<Integer>();
        ArrayList<Integer> l1 = new ArrayList<Integer>();

        c1.add(9);
        c1.add(3);
        c1.add(5);
        c1.add(4);
        l1.add(10);
        l1.add(4);
        l1.add(2);
        l1.add(7);
        testCasIdentiques(c1,l1,false);
        System.out.println("identiques n°2 : ");
        ArrayList<Integer> c2 = new ArrayList<Integer>();
        ArrayList<Integer> l2 = new ArrayList<Integer>();
        
        c2.add(78);
        c2.add(99);
        c2.add(34);
        l2.add(99);
        l2.add(78);
        l2.add(34);
        testCasIdentiques(c2,l2,true);

    }
    
    
    
    
    /**
     * Allows to test several cases of identiques
     * @param copie collection of items
     * @param positionsPerdantes collection of loosing positions
     * @param ret boolean expected after method
     */
    void testCasIdentiques(ArrayList<Integer> copie, ArrayList<Integer> liste,boolean ret){
        // Arrange
        System.out.println ("Copie de la liste : " + copie.toString() +" Liste à comparer : " + liste.toString());
        System.out.println("Résulat attendu : " +ret);
		// Act
        boolean resExec;
        resExec = identiques(copie,liste);
        // Assert			 
        if ( resExec == ret ){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }
	

	
	/**
	 * Method allowing to make a copy of jeu and sort this new list
	 * and delete from this list all stacks of less than 3 matches
	 * @param jeu game board
	 */
	ArrayList<Integer>normalise(ArrayList<Integer>jeu){
		int i = 0;
		ArrayList<Integer>copie = new ArrayList<Integer>();
		while (i < jeu.size()){
			if ( jeu.get(i) > 2){
				copie.add(jeu.get(i));
			}
			i++;
		}
		Collections.sort(copie);
		return copie;
	}
	
	/**
	 * Method allowing to test normalise method
	 */
	void testNormalise(){
		// Basic cases
		System.out.println();
		System.out.println("*** testNormalise : ");
		System.out.println("Normalise n°1 : ");
		ArrayList<Integer>l1 = new ArrayList<Integer>();
		ArrayList<Integer>r1 = new ArrayList<Integer>();
		l1.add(1);
		l1.add(5);
		l1.add(65);
		l1.add(2);
		r1.add(5);
		r1.add(65);
		testCasNormalise(l1,r1);
		System.out.println("Normalise n°2 : ");
		ArrayList<Integer>l2 = new ArrayList<Integer>();
		ArrayList<Integer>r2 = new ArrayList<Integer>();
		l2.add(87);
		l2.add(66);
		l2.add(105);
		r2.add(66);
		r2.add(87);
		r2.add(105);
		testCasNormalise(l2,r2);
	}
	
	/**
	 * Method allowing to test several cases of normalise method
	 * @param jeu game board
	 * @param result list expected after normalise method
	 */
	void testCasNormalise(ArrayList<Integer> jeu, ArrayList<Integer> result){
        // Arrange
        System.out.println ("Liste de départ (" + jeu.toString() +", résultat attendu : "+ result.toString()+")");
		// Act
        ArrayList<Integer> resExec = new ArrayList<Integer>();
        resExec = normalise(jeu);
        System.out.println("Résultat : " + resExec.toString());
        // Assert			 
        if (resExec.equals(result)){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }
	
	
	/**
	 * Method allowing to delete items of a board
	 * if they are already known from sitPerdantes
	 * @param jeu game board
	 * @return a list in which all losing situations are deleted
	 */
	ArrayList<Integer>simplification(ArrayList<Integer> jeu){ 
        ArrayList<Integer>liste1 = new ArrayList<Integer>();
        ArrayList<Integer> ret = normalise(jeu);
        int i = 0;
        
        while( i < ret.size()){
			liste1.clear();
			liste1.add(ret.get(i));
			if (estConnuePerdante(liste1)){
				ret.remove(i);
			}
			i++;
		}
        return ret;
    }
	
	
	/**
	 * Method allowing to test simplification
	 */
	void testSimplification(){
		// Basic cases
		System.out.println("***testSimplification : ");
		System.out.println("Liste n°1 : ");
		ArrayList<Integer>j1 = new ArrayList<Integer>();
		ArrayList<Integer>r1 = new ArrayList<Integer>();
		j1.add(2);
		j1.add(10);
		ArrayList<Integer>p1 = new ArrayList<Integer>();
		ArrayList<Integer>p6 = new ArrayList<Integer>();
		p1.add(2);
		p6.add(10);
		sitPerdantes.add(p1);
		sitGagnantes.add(p6);
		r1.add(10);
		testCasSimplification(j1,r1);
		sitGagnantes.clear();
		sitPerdantes.clear();
		
		System.out.println("Liste n°2 : ");
		ArrayList<Integer>j2 = new ArrayList<Integer>();
		ArrayList<Integer>r2 = new ArrayList<Integer>();
		j2.add(78);
		j2.add(60);
		ArrayList<Integer>p4 = new ArrayList<Integer>();
		p4.add(90);
		p4.add(61);
		r2.add(60);
		r2.add(78);
		sitPerdantes.add(p4);
		testCasSimplification(j2,r2);
		sitPerdantes.clear();
	}
	
	
    
    
    /**
     * Allows to test several cases of simplification
     * @param copie collection of items
     * @param positionsPerdantes collection of loosing positions
     * @param ret integer expected after method
     */
    void testCasSimplification(ArrayList<Integer> liste,ArrayList<Integer>ret){
        // Arrange
        System.out.println ("Liste au départ : " + liste.toString() +" Liste attendu : " + ret.toString());
		// Act
        ArrayList<Integer>resExec = new ArrayList<Integer>();
        resExec = simplification(liste);
        // Assert			 
        if ( resExec.equals(ret)){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
            System.out.println(resExec);
        }
    }
	
	
	
	
	
	
	/**
	 * method allows to test estGagnant method
	 */
	void testEstGagnanteEfficacite(){
		System.out.println();
		System.out.println("testEstGagnanteEfficacité : " );
        boolean indice ;
		int i = 1;
		long t1,t2,diffT;
		int n = 3;
		// Do 50 experiences or less if this is impossible 
		while( i <= 50){
			ArrayList<Integer>jeu = new ArrayList<Integer>();
			jeu.add(n);
			cpt = 0;
			t1 = System.nanoTime();
			indice =estGagnante(jeu);
            System.out.println("indice pour "+n+" : " + indice);
			t2 = System.nanoTime();
			diffT = (t2 - t1)/1000;
			System.out.println("Expérience  : " + i );
			System.out.println("Tps : " + diffT + " µs");
			System.out.println("cpt : " +cpt);
			System.out.println ("n = " +n);
			i++;
			n = n + 1;
			// Empty the loosing collection after each experience
			sitPerdantes.clear();
			// Empty the winning collection after each experience
			sitGagnantes.clear();
		}
	}
	
}

