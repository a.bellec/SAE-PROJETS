/**
* Auteur: Bellec Alan
* jeu de Grundy
*/
import java.util.Arrays;
class Grundy1{
    void principal (){
        System.out.println(" Bienvenue au jeu du Grundy ");
        //testAll();
        partie ();
        
    }
    
    /** Méthode contenant tous les tests*/
    void testAll(){
        testTasDepart ();
        testAffichageTas();
        testchangementJoueur();
        testDiviseTableau ();
        testEstFini();
        
    }
    
    /**
    * Création d'un tableau contenant le nombre de batons de la partie
    * @return tableau contenant le nombre de batons voulus
    */
    int[] tasDepart (){
        
        int nbDepart;
        nbDepart = SimpleInput.getInt(" Entrez le nombre de batons pour"+
                " cette partie (au moins 3) :");
        while (nbDepart < 3){
            nbDepart = SimpleInput.getInt(" Entrez le nombre de batons pour"+
                " cette partie (au moins 3) :");
        }
        int[] tas = new int[nbDepart-1];
        tas[0] = nbDepart;
        
        return tas;
    }
    
    /** Methode test tasDepart */
    void testTasDepart (){
        System.out.println(Arrays.toString(tasDepart()));
    }
    
    /**
     * Change de joueur
     * @param joueur ayant joué
     * @param joueur1
     * @param joueur2
     * @return joueur devant jouer
     */
    String changementJoueur (String joueurActuel, String joueur1, String joueur2){
        if ( joueurActuel == joueur1){
            joueurActuel = joueur2;
        }else {
            joueurActuel = joueur1;
        }
        return joueurActuel;
    }
    
    /** Methode de test de Changementjoueur */
     void testchangementJoueur(){
         System.out.println ();
         System.out.println ("*** testchangementJoueur()");
         testCasChangementJoueur("alan","alan","louis","louis");
         testCasChangementJoueur("louis","alan","louis","alan");
    }
    
    /*** teste un appel de tester
    * @param joueurActuel
    * @param joueur1
    * @param joueur2
    * @param result le resultat attendu
    */
    void testCasChangementJoueur (String joueurActuel, String joueur1,
                                  String joueur2,String result) {
        
        // Arrange
        System.out.print ("changementJoueur (" + joueurActuel +", "+ joueur1 +
            ", "+ joueur2 +")");
		// Act
        String resExec = changementJoueur ( joueurActuel,joueur1, joueur2);	
        // Assert			 
        if (resExec == result){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }
    
    /**
     * Affiche le nombre de batons total
     * @param tableau contenant les différents batons aux différentes lignes
     */
    void affichageTas(int[]tas){
        for(int i=0 ; i < tas.length;i++){
            if (tas[i] !=0){
                System.out.print( i + ": ");
                for (int j=0; j < tas[i];j++){
                    System.out.print("|" + " ");
                }
                System.out.println();
            }
        }
    }
    
    /** Methode de test affichageTas
     */
     void testAffichageTas(){
         System.out.println ();
			 System.out.println ("*** testAffichageTas()");
			 int[] t1 = {8};
             affichageTas (t1);
             int[] t2 = {1,2,3,4,5,6};
			 affichageTas (t2);
             int[] t3 = {0,0,1};
             affichageTas (t3);
    }
    
   /** Permet de diviser le nombre de baton sur une ligne
    * @param tableau contenant le nombre de batons
    * @param ligne ou l'on veut diviser
    * @param nombre de baton que l'on garde sur cette ligne
    */ 
     void diviseTableau( int[]tas,int ligne, int coupe){
         int temp = tas[ligne];
         tas[ligne] = coupe;
         for (int i=0; i< tas.length; i++){
             if (tas[i] == 0){
                 tas[i]= temp - coupe;
                 i = tas.length;
            }
        }   
    }
    
    /**teste de la méthode diviseTableau */
    void testDiviseTableau () {
        System.out.println ();
        System.out.println ("*** testDiviseTableau()");
        int[]t = {7,2,0,0,0,0,0,0};
        int[]t2 = {1,2,6,0,0,0,0,0};
        testCasDiviseTableau(t,0,1,t2);
        int[]t3 = {1,2,1,1,4,0,0,0};
        int[]t4 = {1,2,1,1,3,1,0,0};
        testCasDiviseTableau(t3,4,3,t4);
        int[]t5 = {8,0,0,0,0,0,0,0};
        int[]t6 = {1,7,0,0,0,0,0,0};
        testCasDiviseTableau(t5,0,1,t6);
    }
	
    /*** teste un appel de diviseTableau
    * @param tableau contenant les tas de batons
    * @param ligne ou l'on divise
    * @param coupe ce que l'on souhaite garder sur la ligne
    * @param result le tableau  attendu
    */
    void testCasDiviseTableau (int[] tas, int ligne,int coupe,int[] result) {
        // Act
        diviseTableau(tas,ligne,coupe);
        // Arrange
        System.out.print ("diviseTableau(" +Arrays.toString( tas )+", "+ ligne +", "+ coupe +" )");
        // Assert			 
        if (Arrays.equals(tas,result)){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }
    
    /** Vérifie si au moins un coup est possible
     *@param tableau contenant le nombres de batons aux différentes lignes
     * @return vrai ssi aucun coup n'est possible
     */
     boolean estFini ( int[] tas){
         boolean fini = true;
         for ( int i=0; i < tas.length; i++){
             if ( tas[i] != 0 &&  tas[i] != 1 && tas[i] != 2){
                 fini = false;
            }
        }
        return fini;
    }
    
    /**teste de la méthode estFini
		 */
    void testEstFini() {
        System.out.println ();
        System.out.println ("*** testEstFini()");
        int[]t = {1,2,1,2,1,2,0,0};
        testCasEstFini(t,true);
        int[]t2 = {1,1,1,1,1,1,1,2};
        testCasEstFini(t2,true);
        int[]t3 = {1,1,1,5,0,0,0,0};
        testCasEstFini(t3,false);
        int[]t4 = {1,1,1,1,1,4,0,0};
        testCasEstFini(t4,false);
        int[]t5 = {8,0,0,0,0,0,0,0};
        testCasEstFini(t5,false);
    }
	
    /** teste un appel de estFini
    * @param tableau contenant les batons
    * @param result le resultat attendu
    */
    void testCasEstFini(int[] tas,boolean result) {
        // Arrange
        System.out.print ("estFini(" +Arrays.toString( tas )+" )");
        // Act
        boolean resExec = estFini(tas);
        // Assert			 
        if (resExec==result){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }
    
    void partie (){
        int ligne;
        int coupe;
        int i = 0;
        int[] tas = tasDepart ();
        String joueur1 = SimpleInput.getString(" Entrez le nom du joueur 1 :");
        String joueur2 = SimpleInput.getString(" Entrez le nom du joueur 2 :");
        String joueurActuel = joueur1;
        affichageTas(tas);
        while ( estFini(tas)==false){
            System.out.println(" Tour de : " + joueurActuel);
            if ( i == 0){
                ligne = 0;
            } else {
                ligne = SimpleInput.getInt("Entrez la ligne où vous voulez jouer :");
                // Condition sortie: ligne >=0 && ligne < tas.length && tas[ligne] >= 3
                while (ligne < 0 || ligne >= tas.length || tas[ligne] < 3 ){
                    ligne = SimpleInput.getInt("Entrez la ligne où vous voulez jouer :");
                }
            }
            coupe = SimpleInput.getInt("Entrez le nombre de batons que vous"+ 
                " voulez laissez sur cette ligne :");
            // Condition de sortie : coupe < tas[ligne] && coupe > 0 
            while ( coupe >= tas[ligne] || coupe <= 0 || 
                (tas[ligne] % 2 == 0 && coupe == tas[ligne]/2)){
                coupe = SimpleInput.getInt("Entrez le nombre de batons que vous"+ 
                    " voulez laissez sur cette ligne :");
            }
            diviseTableau(tas,ligne,coupe); 
            joueurActuel = changementJoueur (joueurActuel, joueur1, joueur2);
            affichageTas(tas);
            i = i + 1;
        }
        if ( joueurActuel == joueur1){
            // A la fin du tour du joueur gagnant, il y a changement de joueurActuel
            // Donc le joueur gagnant n'est pas le joueurActuel
            System.out.println(" Bravo à " + joueur2 +" qui remporte la partie !");
        } else{
            System.out.println(" Bravo à " + joueur1 +" qui remporte la partie !");
        }
    }
}

        
