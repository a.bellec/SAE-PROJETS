import java.util.ArrayList;
/**
 * Autors : Alan Bellec, Camille Gouault-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- Larmour
 * 
 */
class Grundy2RecBruteEff {

    long cpt; 
    Grundy2RecBrute monGrundy2RecBrute = new Grundy2RecBrute();

    void principal(){
        partie();


    }

    /**
     * Affiche le nombre de batons total
     * @param jeu contenant les différents batons aux différentes lignes
     */
    void affichageTas(ArrayList<Integer> jeu){

        for(int i = 0 ; i < jeu.size() ; i++){
            if ( jeu.get(i) != 0 ){
                System.out.print( i + " : ");
                for (int j = 0 ; j < jeu.get(i) ; j++){
                    System.out.print("|" + " ");
                }
                System.out.println();
            }
        }

    }

    ArrayList<Integer> jeuDeDepart(){
        
        ArrayList<Integer> jeu = new ArrayList<Integer>();
        int tmp;
        tmp =  SimpleInput.getInt("Entrez le nombre de batons que vous voulez pour la partie (minimum 3) :");
        while ( tmp < 3 ){
            tmp = SimpleInput.getInt("Entrez le nombre de batons que vous voulez pour la partie (minimum 3) :");
        }
        jeu.add(tmp);
        return jeu;
    }

    // METHODE De test jeu de Deoart a faire 

    /**
     * Change de joueur
     * @param joueur ayant joué
     * @param joueur1
     * @param joueur2
     * @return joueur devant jouer
     */
    String changementJoueur (String joueurActuel, String joueur1, String joueur2){
        if ( joueurActuel == joueur1){
            joueurActuel = joueur2;
        }else {
            joueurActuel = joueur1;
        }
        return joueurActuel;
    }
    
    /** Methode de test de Changementjoueur
     */
     void testchangementJoueur(){
         System.out.println ();
         System.out.println ("*** testchangementJoueur()");
         testCasChangementJoueur("alan","alan","louis","louis");
         testCasChangementJoueur("louis","alan","louis","alan");
    }
    
    /*** teste un appel de tester
    * @param joueurActuel
    * @param joueur1
    * @param joueur2
    * @param result le resultat attendu
    */
    void testCasChangementJoueur (String joueurActuel, String joueur1,
                                  String joueur2,String result) {
        
        // Arrange
        System.out.print ("changementJoueur (" + joueurActuel +", "+ joueur1 +
            ", "+ joueur2 +")");
		// Act
        String resExec = changementJoueur ( joueurActuel,joueur1, joueur2);	
        // Assert			 
        if (resExec == result){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }

    void partie(){
        int ligne;
        int nbAllumASeparer;
        ArrayList<Integer> jeu = new ArrayList<Integer> ();
        jeu = jeuDeDepart();
        
        String joueur1 = SimpleInput.getString(" Entrez le nom du joueur 1 :");
        String joueur2 = "Ordinateur";
        String joueurActuel = joueur1;
        affichageTas(jeu);
        while (Grundy2RecBrute.estPossible(jeu)){

        }

    }




}